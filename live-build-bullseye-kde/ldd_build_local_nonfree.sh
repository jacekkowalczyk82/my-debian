#!/bin/bash 


echo "============================"
echo "    container setup START   "
echo "============================"


apt-get update
apt-get install -y neofetch live-build \
 debootstrap \
 isolinux syslinux \
 zip wget curl iputils-ping dnsutils

ping -c 4 deb.debian.org
ping -c 4 security.debian.org


neofetch 


echo "============================"
echo "    container setup DONE    "
echo "============================"




LDD_EDITION="kde-nonfree"

OPTIONS=${1:-NON_INTERACTIVE}
# if -i -> INTERACTIVE

if [ "$OPTIONS" == "-i" ]; then 
    OPTIONS="INTERACTIVE"
fi

confirm() {
    if [[ "$OPTIONS" == "INTERACTIVE" ]]; then 
        read -p "Press any key: " any_key
    else
        echo "OPTIONS: $OPTIONS"
    fi
}


lb clean --purge

lb config --debian-installer live -d bullseye \
 --firmware-chroot true \
 --firmware-binary true \
 --archive-areas "main contrib non-free" \
 --parent-archive-areas "main contrib non-free" \
 --debian-installer-distribution bullseye \
 --apt-options "--yes --option Acquire::Retries=5 --option Acquire::http::Timeout=100" \
 --iso-application debian-live-11-ldd-${LDD_EDITION} \
 --iso-volume debian-live-11-ldd-${LDD_EDITION} \
 --image-name debian-live-11-ldd-${LDD_EDITION} \
 --iso-publisher "Jacek Kowalczyk http://jacekkowalczyk82.github.io" \
 --iso-preparer "Jacek Kowalczyk http://jacekkowalczyk82.github.io" 
 


mkdir -p binary/firmware
cd binary/firmware
wget http://cdimage.debian.org/cdimage/unofficial/non-free/firmware/bullseye/current/firmware.tar.gz
tar -xvzf firmware.tar.gz 
rm firmware.tar.gz 
cd ../.. 

confirm

cp -v live.list.chroot_nonfree ./config/package-lists/live.list.chroot

#add your packages to config/package-lists/live.list.chroot
#add your customization files to config/includes.chroot/

mkdir -p config/includes.chroot/opt/
cp -rv ../suckless.org/dwm ./config/includes.chroot/opt/
#mv -v ./config/includes.chroot/opt/dwm-gaps ./config/includes.chroot/opt/dwm
cp -rv ../src/usr ./config/includes.chroot/

#cp -rv ./customization/bootloaders ./config/
# bullseye artwork
#cp -v ./config/bootloaders/grub-pc/homeworld_grub.png ./config/bootloaders/grub-pc/splash.png 
#cp -v ./config/bootloaders/isolinux/homeworld_sys_iso.png ./config/bootloaders/isolinux/splash.png 

# /usr/share/live/build/bootloaders/isolinux
# /usr/share/live/build/bootloaders/grub-pc/
# /usr/share/live/build/bootloaders/syslinux
# rocessing triggers for man-db (2.9.4-2) ...
#cp: not writing through dangling symlink 'chroot/root/isolinux/hdt.c32'
#cp: not writing through dangling symlink 'chroot/root/isolinux/isolinux.bin'
#cp: not writing through dangling symlink 'chroot/root/isolinux/ldlinux.c32'                              
#cp: not writing through dangling symlink 'chroot/root/isolinux/libcom32.c32'
#cp: not writing through dangling symlink 'chroot/root/isolinux/libgpl.c32'
#cp: not writing through dangling symlink 'chroot/root/isolinux/libmenu.c32'
#cp: not writing through dangling symlink 'chroot/root/isolinux/libutil.c32'
#cp: not writing through dangling symlink 'chroot/root/isolinux/vesamenu.c32'
#E: An unexpected failure occurred, exiting...

confirm


#when rebuilding run also clean
#sudo lb clean --purge

keep_trying=1
command_run_counter=1
while [ $keep_trying -eq 1 ]; do
    echo "lb build try $command_run_counter"
    lb build --debug --verbose 2>&1 |tee debian-live-11-ldd-${LDD_EDITION}-`date '+%Y-%m-%d_%H%M%S'`.log

    LB_EXIT=$?
    if [ $LB_EXIT -eq 0 ]; then 
        keep_trying=0
        echo "lb build try $command_run_counter success"
        break
    else
        echo "lb build try $command_run_counter failed"
    fi
    
    if [ $command_run_counter -gt 3 ]; then 
        keep_trying=0
        break
    fi
    ((command_run_counter++))
done


#`date '+%Y-%m-%d_%H%M%S'`
GENERATED_NAME="debian-live-11-ldd-${LDD_EDITION}-amd64"
RELEASE_NAME="debian-live-11-ldd-${LDD_EDITION}-`date '+%Y-%m-%d_%H%M%S'`-amd64"
RELEASE_DATE=`date '+%Y%m%d'`

if [ -e "${GENERATED_NAME}.hybrid.iso" ]; then 

    mv ${GENERATED_NAME}.hybrid.iso  ${RELEASE_NAME}.iso
    sha256sum ${RELEASE_NAME}.iso | tee -a ${RELEASE_NAME}.sha256sum.txt

    FILES_PATTERN="debian-live-11-ldd-${LDD_EDITION}"

    #aws s3 --region us-east-2 cp ${FILES_PATTERN}-*.log  s3://jacekkowalczyk82.private.s3/my-debian-images/${RELEASE_DATE}/
    #aws s3 --region us-east-2 cp ${RELEASE_NAME}.iso s3://jacekkowalczyk82.private.s3/my-debian-images/${RELEASE_DATE}/
    #aws s3 --region us-east-2 cp ${RELEASE_NAME}.sha256sum.txt s3://jacekkowalczyk82.private.s3/my-debian-images/${RELEASE_DATE}/

    #aws s3 --region us-east-2 cp ${GENERATED_NAME}.contents         s3://jacekkowalczyk82.private.s3/my-debian-images/${RELEASE_DATE}/
    #aws s3 --region us-east-2 cp ${GENERATED_NAME}.files         s3://jacekkowalczyk82.private.s3/my-debian-images/${RELEASE_DATE}/
    #aws s3 --region us-east-2 cp ${GENERATED_NAME}.hybrid.iso.zsync  s3://jacekkowalczyk82.private.s3/my-debian-images/${RELEASE_DATE}/
    #aws s3 --region us-east-2 cp ${GENERATED_NAME}.packages      s3://jacekkowalczyk82.private.s3/my-debian-images/${RELEASE_DATE}/

else 
	echo "ERROR: failed to build ISO"
fi 
