# Building LDD bullseye in docker container 

* prepare 

```
docker pull debian:bullseye

mkdir -p customization/bootloaders

cp -rv ../suckless.org ./customization/
cp -rv ../src ./customization/

cp -rv /usr/share/live/build/bootloaders/isolinux ./customization/bootloaders
cp -rv /usr/share/live/build/bootloaders/grub-pc ./customization/bootloaders
cp -rv /usr/share/live/build/bootloaders/syslinux ./customization/bootloaders

```
* create Dockerfile 

```
FROM debian:bullseye

RUN apt-get update && apt-get -y install git bash neofetch live-build zip wget curl iputils-ping dnsutils

CMD ["/bin/bash"]
```

* run 

```
sudo docker run --privileged -i \
    -v /proc:/proc \
    -v ${PWD}:/working_dir \
    -w /working_dir \
    debian:bullseye \
    /bin/bash < ldd_build_local_nonfree.sh

or 

sudo docker build -t mylb .


sudo docker run --privileged -i \
    -v /proc:/proc \
    -v ${PWD}:/working_dir \
    -w /working_dir \
    mylb \
    /bin/bash < ldd_build_local_nonfree.sh
    
    


sudo docker run --privileged -i \
    -v /proc:/proc \
    -v ${PWD}:/working_dir \
    -w /working_dir \
    mylb lb build --debug --verbose

```
